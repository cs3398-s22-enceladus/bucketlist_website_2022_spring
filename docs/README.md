# Project Name (5 pts)
# Event Mates 

# Description:
## Answer the following 4 “vision” questions:

## Who you’re working with (you and your team members)? 

### Ertugrul Oksuz, Jordan Finney, Kaizhao Cheng, Zechariah Johnson


## What you’re creating?

### A bucket list website that can be used individually or within groups to connect people with similar interests, akin to a type of social media website. This website will be specifically focused towards the San Marcos area.


## Who you’re doing it for, your audience (may be same as the previous question)?

### Our audience is the San Marcos area, preferably University students and people who are new to the area. However, any resident of San Marcos is encouraged to make use of the website to further their community involvement.


## Why are you doing this, what is the impact or change you hope to make?

### We are doing this to further our programming knowledge and software engineering skills not only as individuals,
### but also (and arguably more importantly) as a highly productive team.

### Our hope is that people will feel more comfortable exploring their city and meeting people.

### We also aim to connect people with similar interests in (specific) activities to promote a more active and socially 
### engaged community in San Marcos. 


# General Info (5 pts): For this part:

## Create an image/picture/icon representing your project.  Have fun but be nice (please).   You can change the image 
## as we go through the semester. Optional, but if you feel like it, what’s missing from your
## project (expertise, users, additional developers, money☺, etc) that you wish you had.
<p align="center">
  <img src="https://i.imgur.com/pSrQous.png"  width="350" title="project icon">

</p>

 
# Technologies (5 pts): : List the technologies you will be using.  
## If you can hyperlink them to webpages describing the technologies that is a plus.

### HTML/CSS/(Java Script)

### Mozilla Firefox/Google Chrome

### Angular framework/RxJS

### TypeScript

### Misc. technologies presented in class (GitKraken, CircleCi, BitBucket, Git, etc.)

 
# Features (25 pts):  What will be the initial features in this first sprint.
# Give each feature a name.

### User Profiles

### Bucket List per profile

### Search Functions (auto-fill) (High priority)

#### If First three are done with ease next step is to

##### Add Friends/Maintain friends list
##### Group Event (demo) (low priority)


## Describe what it does and who or what uses it.

### The user can add items into their bucket list, mark them as completed
### They can add their friends on the website.


# What user stories correspond to this feature.

# Feature Name: User Profiles 

 This feature is a user profile where users can create a profile for themselves. This will consist of a username, the abilities to see their own bucket list, bio, profile picture, attended/interested events, corresponding details, and miscellaneous details specific to that user. The user profiles will be implemented as a tool to be used extensively among other users to identity and correspond a user with their real-life identity

 This feature corresponds to the story idea by Ertugrul Oksuz: as a student in Texas State I would love to create a profile that would help me keep track of my bucket lists when I explore more of the city San Marcos.

# Feature Name: Individual Bucket List

 This Feature is a bucket list feature for the individual user that can add things that they would like to do and achieve within a certain time frame the user sets. The goal with this feature is to add website activity by the user to see and mark off what they want to do or achieve.

 This feature corresponds to the story idea by Jordan Finney which says: I, Jordan, as a member of development would like to add your own bucket list to promote productivity in the app. This feature will be the backbone for later relational searches/features that lead from individual bucket lists to group bucket lists and group activities. We predict this will increase website activity even more by the user as they will meet new people or set goals to achieve with their friends rather than only individual goals.

# Feature Name: Search Functions

 This feature is a search feature where the user can type and search for different events/people/challenges/etc. This feature is intended to be used by virtually every user who will be using the website. The goal with this feature is to make a functional search feature/organizational tool that can be ‘slotted into’ each and every different category of searches e.g. Community Event searches, Friend searches, Challenge Searches, BucketList Item searches, etc.

 This feature corresponds with the user story submitted by the author Zechariah Johnson which states: I, Zechariah, as a basic newcomer BucketList app/software user, want some sort of feature that connects me and alerts me about other users who have similar bucketlist goals/completions around San Marcos so that I can connect, communicate, and collaborate with people who enjoy likewise activities. This user story has evolved to be the backbone for the search function feature. The “alert” part of the user story can be broken off of this feature and moved to some other sort of feature if need be. The search function will be serving as the requested feature to connect others with similar bucketlist goals/completions around San Marcos. This connection will allow users to communicate and collaborate with other users/events. Although the user story only requests bucketlist goals/completions specifically, we decided as a team that we should extend this feature to spread through the entire website’s framework so that it can be used for any and all searchable categories we choose to include.

# Sprint 1 Report
## Zechariah Johnson: 
### Contributions

### 1. Corresponding Jira Task:  Study up on basic HTML/CSS/JavaScript practices
 Followed the beginning videos from the following course on Udemy:

* https://www.udemy.com/course/the-complete-web-development-bootcamp/

 Went up to Section 2, through video 16

* Set up personal development environment for the website (atom.io)
* Studied basic structure of HTML and CSS documents
* This allowed me to fluidly understand the basics of these document types, which then opens the door for implementing Google API services into our website.

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/HTML-CSS_research_zj.txt

### 2. Corresponding Jira Task:  This Bucket List should include a way to tie certain (or all) bucket list items to geographic locations around San Marcos. This could involve pulling data from Google, Apple, Waze, etc. to tie geographic location to events/bucket list items
Activated Google API services for 5+ different various APIs including:

* Directions API
* Geocoding API
* Geolocation API
* Maps JavaScript API
* Places API

These Google services will allow us to implement future features onto our current myMap.html file and/or new map objects for different places in our website. These future features include but are not limited to:

* Placing Markers on the map
* Displaying information about a marker's set location
* Retrieving the current GPS location of the user
* Setting routes within the Google Map to direct a user to a desired destination
* Placing highlighted polygons across desirable locations on various Google Maps
* Adding a search bar to the Google Map
* Many many more miscellaneous Google Map features to optimize the user's experience of the Google Maps across our website.

Link(s) to the corresponding documentation in the BitBucket repository: 

* Desired API features to implement - https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/google_DocumentationFramework_ReferenceSheet.txt
* API Key to be later obfuscated: https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/API_KEY_GOOGLE.txt

### Next Steps for Sprint 2:

Begin implementing various features related to Google Maps API services including:

* Placing Markers on the map
* Displaying information about a marker's set location
* Retrieving the current GPS location of the user
* Setting routes within the Google Map to direct a user to a desired destination
* If I can accomplish the above features relatively quickly I will move on to implementing quality-of-life features to our Google Maps to enhance the user experience
* I would also like to begin connecting these Google Maps to my team member's various feature contributions and placing them in their respective areas of the website

## Jordan Finney:
### Contributions

### 1. Corresponding Jira Task: Research HTML and CSS 
 Followed the beginning videos from the following course on Udemy:

* https://www.udemy.com/course/the-complete-web-development-bootcamp/

Watched up to Section 5, video 46

* Made the mainframe of our website using Repl.it 
* Researched HTML and CSS based on the Udemy team account.
* This allowed me to use full creativity in our website and expolre the boundries of internet hosted sites.

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/HTML%20Research

### 2. Correspinding Jira Task: Upload HTML website as is
This included a whole file of HTML site files 

* about.html
* contactus.html
* featuredevents.html
* forgotpassword.html
* myevents.html
* mymates.html
* newaccount.html 

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/pages/

### Next Steps for Sprint 2: 

Begining the visual stages of our main pages: 

* Getting with Zach to start putting the map on the home page 
* Work with Michael to start implementing his working hash table into the login page 
* Work with Kai to see how to start making a visual for bucketlist 
* Create a visual page for the "Featured Events" page 
* Create a visual chat feature 
* Create a visual posting system with likes, comments, etc... 

## Ertugrul Oksuz
### Contributions:

### 1. Corresponding Jira Task:Study up on basic HTML/JavaScript relearn hashing to write it in JavaScript
 Followed the beginning videos from the following course on Udemy:

* https://www.udemy.com/course/the-complete-web-development-bootcamp/

Watched up to Section 2, video 14

Also Researched basic ways to code on JavaScript learning the differences between Java and JavaScript through youtube and stackoverflow

* This allowed me to learn and understand how to write the Hashtable to store user information on our website.

### 2. Corresponding Jira Task: Creating a class that stores user profiles, personal info (name, last name), and personal interests
This included a file of JavaScript

* index.js

Which did the following

* Made the Hashtable to store user information
* Added users into the Hashtable
* Made the Hashtable search through the information of the user to find password, or personal interests.

Link to the corresponding documentation in the BitBucket repository:

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/index.js

### Next Steps for Sprint 2:

Begin storing user data to be visible to the user upon login

* Creating an sql database to store user information from our website
* Organize/Breakup the HashTable class into the storing and searching classes of the HashTable
* Have a running code to store user bucketlists per individual user
* Working with Jordan to implement the working hash table into the login page
* Working with Jordan to implement user bucketlist on the individual side of the webpage
* Working with Zechariah to implement the search function to find usernames/ emails
