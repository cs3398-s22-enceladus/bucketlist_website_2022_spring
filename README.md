# Project Name (5 pts)
# Event Mates 

# Description:
## Answer the following 4 “vision” questions:

## Who you’re working with (you and your team members)? 

### Ertugrul Oksuz, Jordan Finney, Kaizhao Cheng, Zechariah Johnson


## What you’re creating?

### A bucket list website that can be used individually or within groups to connect people with similar interests, akin to a type of social media website. This website will be specifically focused towards the San Marcos area.


## Who you’re doing it for, your audience (may be same as the previous question)?

### Our audience is the San Marcos area, preferably University students and people who are new to the area. However, any resident of San Marcos is encouraged to make use of the website to further their community involvement.


## Why are you doing this, what is the impact or change you hope to make?

### We are doing this to further our programming knowledge and software engineering skills not only as individuals,
### but also (and arguably more importantly) as a highly productive team.

### Our hope is that people will feel more comfortable exploring their city and meeting people.

### We also aim to connect people with similar interests in (specific) activities to promote a more active and socially 
### engaged community in San Marcos. 


# General Info (5 pts): For this part:

## Create an image/picture/icon representing your project.  Have fun but be nice (please).   You can change the image 
## as we go through the semester. Optional, but if you feel like it, what’s missing from your
## project (expertise, users, additional developers, money☺, etc) that you wish you had.
<p align="center">
  <img src="https://i.imgur.com/pSrQous.png"  width="350" title="project icon">

</p>

 
# Technologies (5 pts): : List the technologies you will be using.  
## If you can hyperlink them to webpages describing the technologies that is a plus.

### HTML/CSS/(Java Script)

### Mozilla Firefox/Google Chrome

### Angular framework/RxJS

### TypeScript

### Misc. technologies presented in class (GitKraken, CircleCi, BitBucket, Git, etc.)

 
# Features (25 pts):  What will be the initial features in this first sprint.
# Give each feature a name.

### User Profiles

### Bucket List per profile

### Search Functions (auto-fill) (High priority)

#### If First three are done with ease next step is to

##### Add Friends/Maintain friends list
##### Group Event (demo) (low priority)


## Describe what it does and who or what uses it.

### The user can add items into their bucket list, mark them as completed
### They can add their friends on the website.


# What user stories correspond to this feature.

# Feature Name: User Profiles 

 This feature is a user profile where users can create a profile for themselves. This will consist of a username, the abilities to see their own bucket list, bio, profile picture, attended/interested events, corresponding details, and miscellaneous details specific to that user. The user profiles will be implemented as a tool to be used extensively among other users to identity and correspond a user with their real-life identity

 This feature corresponds to the story idea by Ertugrul Oksuz: as a student in Texas State I would love to create a profile that would help me keep track of my bucket lists when I explore more of the city San Marcos.

# Feature Name: Individual Bucket List

 This Feature is a bucket list feature for the individual user that can add things that they would like to do and achieve within a certain time frame the user sets. The goal with this feature is to add website activity by the user to see and mark off what they want to do or achieve.

 This feature corresponds to the story idea by Jordan Finney which says: I, Jordan, as a member of development would like to add your own bucket list to promote productivity in the app. This feature will be the backbone for later relational searches/features that lead from individual bucket lists to group bucket lists and group activities. We predict this will increase website activity even more by the user as they will meet new people or set goals to achieve with their friends rather than only individual goals.

# Feature Name: Search Functions

 This feature is a search feature where the user can type and search for different events/people/challenges/etc. This feature is intended to be used by virtually every user who will be using the website. The goal with this feature is to make a functional search feature/organizational tool that can be ‘slotted into’ each and every different category of searches e.g. Community Event searches, Friend searches, Challenge Searches, BucketList Item searches, etc.

 This feature corresponds with the user story submitted by the author Zechariah Johnson which states: I, Zechariah, as a basic newcomer BucketList app/software user, want some sort of feature that connects me and alerts me about other users who have similar bucketlist goals/completions around San Marcos so that I can connect, communicate, and collaborate with people who enjoy likewise activities. This user story has evolved to be the backbone for the search function feature. The “alert” part of the user story can be broken off of this feature and moved to some other sort of feature if need be. The search function will be serving as the requested feature to connect others with similar bucketlist goals/completions around San Marcos. This connection will allow users to communicate and collaborate with other users/events. Although the user story only requests bucketlist goals/completions specifically, we decided as a team that we should extend this feature to spread through the entire website’s framework so that it can be used for any and all searchable categories we choose to include.

# Sprint 1 Report
## Zechariah Johnson: 
### Contributions

### 1. Corresponding Jira Task:  Study up on basic HTML/CSS/JavaScript practices
 Followed the beginning videos from the following course on Udemy:

* https://www.udemy.com/course/the-complete-web-development-bootcamp/

 Went up to Section 2, through video 16

* Set up personal development environment for the website (atom.io)
* Studied basic structure of HTML and CSS documents
* This allowed me to fluidly understand the basics of these document types, which then opens the door for implementing Google API services into our website.

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/HTML-CSS_research_zj.txt

### 2. Corresponding Jira Task:  This Bucket List should include a way to tie certain (or all) bucket list items to geographic locations around San Marcos. This could involve pulling data from Google, Apple, Waze, etc. to tie geographic location to events/bucket list items
Activated Google API services for 5+ different various APIs including:

* Directions API
* Geocoding API
* Geolocation API
* Maps JavaScript API
* Places API

These Google services will allow us to implement future features onto our current myMap.html file and/or new map objects for different places in our website. These future features include but are not limited to:

* Placing Markers on the map
* Displaying information about a marker's set location
* Retrieving the current GPS location of the user
* Setting routes within the Google Map to direct a user to a desired destination
* Placing highlighted polygons across desirable locations on various Google Maps
* Adding a search bar to the Google Map
* Many many more miscellaneous Google Map features to optimize the user's experience of the Google Maps across our website.

Link(s) to the corresponding documentation in the BitBucket repository: 

* Desired API features to implement - https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/google_DocumentationFramework_ReferenceSheet.txt
* API Key to be later obfuscated: https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/API_KEY_GOOGLE.txt

### Next Steps for Sprint 2:

Begin implementing various features related to Google Maps API services including:

* Placing Markers on the map
* Displaying information about a marker's set location
* Retrieving the current GPS location of the user
* Setting routes within the Google Map to direct a user to a desired destination
* If I can accomplish the above features relatively quickly I will move on to implementing quality-of-life features to our Google Maps to enhance the user experience
* I would also like to begin connecting these Google Maps to my team member's various feature contributions and placing them in their respective areas of the website

## Jordan Finney:
### Contributions

### 1. Corresponding Jira Task: Research HTML and CSS 
 Followed the beginning videos from the following course on Udemy:

* https://www.udemy.com/course/the-complete-web-development-bootcamp/

Watched up to Section 5, video 46

* Made the mainframe of our website using Repl.it 
* Researched HTML and CSS based on the Udemy team account.
* This allowed me to use full creativity in our website and expolre the boundries of internet hosted sites.

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/HTML%20Research

### 2. Corresponding Jira Task: Create UI Mockups 
In this task I used Canva.com to create UI mockups, these are subject to change as we progress into the creatation of our website.

Link(s) to corresponding documentation in the BitBucket repository:

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/The%20Evolution%20of%20Our%20EventMates%20Website.docx

### 3. Corresponding Jira Task: Create a simple button layout to test
In this task I used my research to create "tabs" and it is a workable function to move between website pages

This included a whole file of HTML site files 

* about.html
* contactus.html
* featuredevents.html
* forgotpassword.html
* myevents.html
* mymates.html
* newaccount.html 

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/pages/

### 4. Corresponding Jira Task: Create About and Contact Us page 
This included HTML site files 

* about.html
* contactus.html

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/pages/

### 5. Corresponding Jira Task: Upload HTML website as is
This included a whole file of HTML site files 

* about.html
* contactus.html
* featuredevents.html
* forgotpassword.html
* myevents.html
* mymates.html
* newaccount.html 

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/pages/

### Next Steps for Sprint 2: 

Begining the visual stages of our main pages: 

* Getting with Zach to start putting the map on the home page 
* Work with Michael to start implementing his working hash table into the login page 
* Work with Kai to see how to start making a visual for bucketlist 
* Create a visual page for the "Featured Events" page 
* Create a visual chat feature 
* Create a visual posting system with likes, comments, etc... 


## Ertugrul Oksuz
### Contributions:

### 1. Corresponding Jira Task:Study up on basic HTML/JavaScript relearn hashing to write it in JavaScript
 Followed the beginning videos from the following course on Udemy:

* https://www.udemy.com/course/the-complete-web-development-bootcamp/

Watched up to Section 2, video 14

Also Researched basic ways to code on JavaScript learning the differences between Java and JavaScript through youtube and stackoverflow

* This allowed me to learn and understand how to write the Hashtable to store user information on our website.

### 2. Corresponding Jira Task: Creating a class that stores user profiles, personal info (name, last name), and personal interests
This included a file of JavaScript

* index.js

Which did the following

* Made the Hashtable to store user information
* Added users into the Hashtable
* Made the Hashtable search through the information of the user to find password, or personal interests.

Link to the corresponding documentation in the BitBucket repository:

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/index.js

### Next Steps for Sprint 2:

Begin storing user data to be visible to the user upon login

* Creating an sql database to store user information from our website
* Organize/Breakup the HashTable class into the storing and searching classes of the HashTable
* Have a running code to store user bucketlists per individual user
* Working with Jordan to implement the working hash table into the login page
* Working with Jordan to implement user bucketlist on the individual side of the webpage
* Working with Zechariah to implement the search function to find usernames/ emails

## Kaizhao Cheng:
### Contributions

### 1. Corresponding Jira Task: Research and studing for task "Implement a messaging feature between user profiles so that they can keep in touch with each other."

The research is from the links:
* https://itsourcecode.com/free-projects/jsprojects/solved-how-to-make-a-live-chat-in-javascript-with-source-code/
* https://code.tutsplus.com/tutorials/how-to-create-a-simple-web-based-chat-application--net-5931
* https://medium.com/@folkertjanvanderpol/creating-a-simple-browser-chat-application-with-socket-io-ee0dce6f0a2b

### 2.Corresponding Jira Task: Studing for Login and register for web page for task "Creating a way to store username, email/login information (hash tables)"

followed the video from the under link, which not in English

* https://www.bilibili.com/video/BV1Xh41167Mv
* https://www.bilibili.com/video/BV1Zk4y127b5

### Next Steps for Sprint 2:

* Create the regiseter code to save the data into database from Ertugrul Oksuz.
* Create the test for connecting code.
* Might start to implement the message chat function
* Working with Jordan to let the test function connect with main web.



# Sprint 2 Report
### Jordan Finney

### 1. Corresponding Jira Task: Research CSS 
In this task I expanded my knowledge on CSS and updated my orginal CSS notes 

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/EventMates%20CSS%20Research.docx

### 2. Corresponding Jira Task: Format About Page 
This included the following files...

Link(s) to corresponding documentation in the BitBucket repository: 

* about.html

### 3. Corresponding Jira Task: Make Layout on Events Page 
This included the following files... 

Link(s) to corresponding documentation in the BitBucket repository: 

* index.html

### 4. Make Add Button on Events Page 
This included the following files... 

Link(s) to corresponding documentation in the BitBucket repository: 

* index.html

### Next Steps for Sprint 3: 

Improve functionality and format:

* Get API Map to display 
* Add edit feature on My Events Page 
* Create button to add featured to events page
* Add ratings to the featured events 

## Ertugrul Oksuz
### 1. Corresponding Jira Task: Research API files uploaded by Zech to figure out Google Maps API
This included the files uploaded by Zechariah under the BitBucket Respository:

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/docs/

In this task I looked through the files uploaded by Zech in order to get an understanding on how to implement the Google Maps API.

*  However there needs to be further research conducted in order to achieve what we want for sprint 3 due to some features missing from the research along with our framework changing for the project

### 2. Corresponding Jira Task: Add a list of events/activities to do around the San Marcos Area to be seen on the Featured Events Page
This included the following file

* home.html

Link to the corresponding documentation in the BitBucket repository:

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/psyWebsite/CleanFragrantApplicationsuite/templates/home.html

### 3. Corresponding Jira Task: Categorize the events into separate sections
This included the following file

* home.html

Link to the corresponding documentation in the BitBucket repository:

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/psyWebsite/CleanFragrantApplicationsuite/templates/home.html

### 4. Corresponding Jira Task: Add visuals for the featured event
This included the following file

* home.html

Link to the corresponding documentation in the BitBucket repository:

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/psyWebsite/CleanFragrantApplicationsuite/templates/home.html

The code for these 3 tasks are all intertwined with each other which leads to the upload of the same document for all 3 tasks. Althought they all cover one side of the task they all work under the same file with eachother.

### Next Steps for Sprint 3: 

Improve functionality and format:

* Get API Map to display with our Featured events if we can
* Add edit feature for individual events under My Events
* Create button to add Featured event from home page into individuals My events page
* Add reviews for each individual featured event

# Sprint 3 Report 
### Jordan Finney 

### 1. Corresponding Jira Task: Add Ratings to Featured Events  
In this task I added Google Ratings to each featured event 

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/Sprint%203/home%20(1).html

* home.html

### 2. Corresponding Jira Task: Make Map API Display 
This included the following files...

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/Sprint%203/home%20(1).html

* home.html
* index.js
* style.css
* index.ts

### 3. Corresponding Jira Task: Add more design 
This included the following files... 

Link(s) to corresponding documentation in the BitBucket repository: 

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/Sprint%203/index.html

* index.html

### Next Steps for Sprint: 

Improve functionality and format:

* Make an edit feature to my events 
* make login 
* make database 
* make more interavtive map api 


### Ertugrul Oksuz
### 1. Corresponding Jira Task: Add a button that allows user to add Featured event from Home page to their own personal list
This included the following file

* home.html

Link to the corresponding documentation in the BitBucket repository:

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/psyWebsite/CleanFragrantApplicationsuite/templates/home.html

### 2. Corresponding Jira Task: Make each event pop up on the Google Maps API so user can click and view address, etc.
This included the following file

* index.js

Link to the corresponding documentation in the BitBucket repository:

* https://bitbucket.org/cs3398-s22-enceladus/bucketlist_website_2022_spring/src/master/src/Sprint%203/index.js

This allowed the user to see the events location pop up on the google maps api on the home screen

### For Next Sprint

Improve functionality and format:

* Make an edit feature to my events 
* Make the google maps api more functionable
* Get the name of the event and send it to my events list without having to type it out
* Make more interavtive map api 