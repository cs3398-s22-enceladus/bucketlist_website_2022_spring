/*
// Initialize and add the map
function initMap() {
  // The location of Uluru
  const uluru = {lat: 29.8833, lng: -97.9414};
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 14,
    center: uluru,
  });
  // The marker, positioned at Uluru
  const marker = new google.maps.Marker({
    position: uluru,
    map: map,
  });
}
*/

// The following example creates five accessible and
// focusable markers.
function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 13,
    center: { lat: 29.8833, lng: -97.9414 },
  });
  // Set LatLng and title text for the markers. The first marker (Boynton Pass)
  // receives the initial focus when tab is pressed. Use arrow keys to
  // move between markers; press tab again to cycle through the map controls.
  const tourStops = [
    [{ lat: 29.86577406586031, lng: -97.94446073131127 }, "Texas Roadhouse"],
    [{ lat: 29.886837108296778, lng: -97.91986746492425 }, "Saltgrass Steak House"],
    [{ lat: 29.824871237181902, lng: -97.97605914988245 }, "Cracker Barrel"],
    [{ lat: 29.886545910622473, lng:  -97.92349203608887 }, "Evo Entertainment Springtown"],
    [{ lat: 29.852284434438477, lng: -97.94987074958364 }, "Evo Cinemas Starplex"],
    [{ lat: 29.886545910622473, lng:  -97.92349203608887 }, "Evo Entertainment Springtown Lanes"],
    [{ lat: 29.85878501908542, lng: -97.93913465938239}, "Sunset Bowling"],
    [{ lat: 29.881552427347703, lng: -97.93970929135084 }, "Cats Billards"],
    [{ lat: 29.886545910622473, lng:  -97.92349203608887 }, "Evo Entertainment Springtown"],
    [{ lat: 29.894158917027198, lng: -97.92966501251517 }, "Meadows Center Glass-Bottom Boat Tours"],
    [{ lat: 29.88620348444583, lng: -97.93509627130763 }, "San Marcos Lions Club Tube Rental"],
    [{ lat: 29.888163918155616, lng: -97.93401206066784 }, "Sewell Park"],
    [{ lat: 29.87876120610064, lng: -97.93396152910235 }, "Rio Vista Park"],
    [{ lat: 29.86883353705275, lng: -97.92989724232928 }, "Stokes Park"],
    [{ lat: 29.883901639599017, lng: -97.94064964532645 }, "The Taproom"],
    [{ lat: 29.883233029273146, lng: -97.93978718950326 }, "Mayloos"],
    [{ lat: 29.879320867715094, lng: -97.93931543253039 }, "Putt Pub"],
    [{ lat: 29.882341440776578, lng: -97.9404410299851 }, "The Marc"],
    [{ lat: 29.88240222661412, lng: -97.94143968372383 }, "Nephew’s"],
    [{ lat: 29.881859641855677, lng: -97.94145260299727 }, "Rooftop on the Square"],
  ];

  // Create an info window to share between markers.
  const infoWindow = new google.maps.InfoWindow();

  // Create the markers.
  tourStops.forEach(([position, title], i) => {
    const marker = new google.maps.Marker({
      position,
      map,
      title: `${i + 1}. ${title}`,
      label: `${i + 1}`,
      optimized: false,
    });

    // Add a click listener for each marker, and set up the info window.
    marker.addListener("click", () => {
      infoWindow.close();
      infoWindow.setContent(marker.getTitle());
      infoWindow.open(marker.getMap(), marker);
    });
  });
}

window.initMap = initMap;