// Initialize and add the map
function initMap(): void {
  // The location of Uluru
  const uluru = { lat: 29.8833, lng: -97.9414 };
  // The map, centered at Uluru
  const map = new google.maps.Map(
    document.getElementById("map") as HTMLElement,
    {
      zoom: 14,
      center: uluru,
    }
  );

  // The marker, positioned at Uluru
  const marker = new google.maps.Marker({
    position: uluru,
    map: map,
  });
}