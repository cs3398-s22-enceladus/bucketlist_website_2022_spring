"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from todo.views import todoView, addTodo, deleteTodo, coverImage
from .import index
"""added path for two html page"""
#added the urlpatterns for all in index.py
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',todoView,name='Myevents'),
    path('addTodo/', addTodo),
    path('deleteTodo/<int:todo_id>/', deleteTodo),
    path('app.png', coverImage),
    path('AboutUs',index.AboutUs),# we can switch to about us page by <a href="AboutUs">About Us</a> code now becaues this path
    path('FeatEve',index.FeatureEves),
    path('ContUs',index.ContactUs),
    path('Home',index.Home),
    path('Map',index.map),
    
]