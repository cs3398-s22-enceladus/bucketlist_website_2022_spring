var HashTable = function() {
  this._storage = [];
  this._count = 0;
  this._limit = 8;
}


HashTable.prototype.insert = function(userName, password, email, name, interests) {

  var index = this.hashFunc(userName, this._limit);

  var bucket = this._storage[index]

  if (!bucket) {
    // Create the bucket
    var bucket = [];
    // Insert the bucket into our hashTable
    this._storage[index] = bucket;
  }

  var override = false;

  for (var i = 0; i < bucket.length; i++) {
    var tuple = bucket[i];
    if (tuple[0] === userName) {

      // Override value stored at this userName
      tuple[1] = password;
      tuple[2] = email;
      tuple[3] = name;
      tuple[4] = interests;
      override = true;
    }
  }

  if (!override) {

    bucket.push([userName, password, email, name, interests]);
    this._count++


    if (this._count > this._limit * 0.75) {
      this.resize(this._limit * 2);
    }
  }
  return this;
};

//needs work doesn't remove item properly
HashTable.prototype.remove = function(userName) {
  var index = this.hashFunc(userName, this._limit);
  var bucket = this._storage[index];
  if (!bucket) {
    return null;
  }

  // Iterate over the bucket
  for (var i = 0; i < bucket.length; i++) {
    var tuple = bucket[i];

    // Check to see if userName is inside bucket
    if (tuple[0] === userName) {

      // If it is, get rid of this tuple
      bucket.splice(i, 1);
      this._count--;
      if (this._count < this._limit * 0.25) {
        this._resize(this._limit / 2);
      }
      return tuple[2];
    }
  }
};


HashTable.prototype.retrieve = function(userName) {
  var index = this.hashFunc(userName, this._limit);
  var bucket = this._storage[index];

  if (!bucket) {
    return null;
  }

  for (var i = 0; i < bucket.length; i++) {
    var tuple = bucket[i];
    if (tuple[0] === userName) {
      return tuple[2];
    }
  }

  return null;
};


HashTable.prototype.hashFunc = function(str, max) {
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    var letter = str[i];
    hash = (hash << 5) + letter.charCodeAt(0);
    hash = (hash & hash) % max;
  }
  return hash;
};


HashTable.prototype.resize = function(newLimit) {
  var oldStorage = this._storage;

  this._limit = newLimit;
  this._count = 0;
  this._storage = [];

  oldStorage.forEach(function(bucket) {
    if (!bucket) {
      return;
    }
    for (var i = 0; i < bucket.length; i++) {
      var tuple = bucket[i];
      this.insert(tuple[0], tuple[1], tuple[2], tuple[3], tuple[4]);
    }
  }.bind(this));
};


HashTable.prototype.retrieveAll = function() {
  console.log(this._storage);
  //console.log(this._limit);
};


var hashT = new HashTable();

hashT.insert('mastereltigre', 'cancel', 'masterltigre@gmail.com', 'ertugrul', 'soccer');
hashT.insert('jc66', 'csProje', 'jordan@gmail.com', 'jordan', 'gaming');


hashT.insert('kai123', '120-589-1970', 'kai@gmail.com', 'kai', 'reading');
hashT.insert('Rick Mires', '520-589-1970', 'zech@txstate.edu', 'zech', 'hiking');
hashT.insert('Tom Bradey', '520-589-1970' , 'esra');
hashT.insert('Biff Tanin', '520-589-1970', 'muam');

hashT.insert('Rick Mires', '520-589-1970', 'zech@txstate.edu', 'zech', 'hiking');
hashT.insert('Tom Bradey', '818-589-1970', 'esra');
hashT.insert('Biff Tanin', '987-589-1970', 'muam');


/*
remove function needs work doesn't remove properly yet
hashT.remove('Rick Mires');
hashT.remove('Tom Bradey');
*/

hashT.insert('Dick Mires', '650-589-1970', 'random');
hashT.insert('Lam James', '818-589-1970', 'anotherran');
hashT.insert('Ricky Ticky Tavi', '987-589-1970', 'thirdran');



hashT.retrieveAll();


console.log(hashT.retrieve('Lam James'));  // 818-589-1970
console.log(hashT.retrieve('Tom Bradey'));
console.log(hashT.retrieve('Boo Radley'));
console.log(hashT.retrieve('Alex Hawkins'));

console.log(hashT.retrieve('Dick Mires')); // 650-589-1970
console.log(hashT.retrieve('Ricky Ticky Tavi')); //987-589-1970
console.log(hashT.retrieve('mastereltigre')); // 510-599-1930
console.log(hashT.retrieve('Lebron James')); // null
